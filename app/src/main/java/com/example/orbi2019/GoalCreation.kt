package com.example.orbi2019

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.orbi2019.commons.inflate
import com.example.orbi2019.networking.IGoalApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime

class GoalCreation : Fragment() {

    var goalApi: IGoalApi? = null

    private fun initGoalApi() {
        goalApi = Retrofit.Builder()
            .baseUrl("http://m.p9gehka.xyz/goal/add/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(IGoalApi::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        val view = inflater.inflate(R.layout.fragment_goal_creation, container, false)
        return container?.inflate(R.layout.fragment_goal_creation)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initGoalApi()

        val createGoal = view.findViewById<Button>(R.id.createGoalButton)
        val goalType = view.findViewById<EditText>(R.id.inputGoalName)
        val goalDays = view.findViewById<EditText>(R.id.inputGoalDay)

        createGoal.setOnClickListener {
            try {
                val currentDateR = LocalDateTime.now()
                val addDays = goalDays.text.toString().toInt()
                val currentDate = currentDateR.plusDays(addDays.toLong())

                CoroutineScope(Main).launch {
                    addGoal(
                        goalType.text.toString(),
                        getDataInt("db", activity as DashboardActivity),
                        currentDateR.toString(),
                        currentDate.toString()
                    )
                }
            } catch (ex: Exception) {
                Toast.makeText(view.context, "Введите корректные данные", Toast.LENGTH_SHORT).show()
                Log.println(Log.ERROR, "[MyError]", ex.message.toString())
            }
        }

    }

    private suspend fun addGoal(
        type: String,
        author: Int,
        startTime: String,
        deadLine: String
    ) {
        try {
            val response = goalApi?.addGoalAsync(
                type,
                author,
                startTime,
                deadLine
            )?.await()
            println("[Goal] add goal response -> " + response?.id)
        } catch (he: HttpException) {
            println("http exception при попытке отправить тип: " + type
                    + ", автор: " + author
                    + ", начало " + startTime
                    + ", дедлайн: " + deadLine)
        }
    }
}
