package com.example.orbi2019

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import androidx.appcompat.app.AppCompatActivity


fun saveDataVk(token: String, vkId: Int, activity: Activity) {
    val sharedPreferences =
        activity.getSharedPreferences("sharedPrefs", AppCompatActivity.MODE_PRIVATE)
    val editor = sharedPreferences.edit()

    editor.putString("VkToken", token)
    editor.putInt("VkId", vkId)
    editor.apply()
}


fun saveUseLogin(login: String, activity: Activity) {
    val sharedPreferences =
        activity.getSharedPreferences("sharedPrefs", AppCompatActivity.MODE_PRIVATE)
    val editor = sharedPreferences.edit()

    editor.putString("login", login)
    editor.apply()
}

fun getDataInt(name: String, activity: Activity): Int {
    val sharedPreferences = activity.getSharedPreferences("sharedPrefs", MODE_PRIVATE)
    return sharedPreferences.getInt(name, -1)
}

fun getDataString(name: String, activity: Activity): String {
    val sharedPreferences = activity.getSharedPreferences("sharedPrefs", MODE_PRIVATE)
    return sharedPreferences.getString(name, "-1")!!
}

fun deleteAllSaveData(activity: Activity) {
    val sharedPreferences = activity.getSharedPreferences("sharedPrefs", MODE_PRIVATE)
    sharedPreferences.edit().clear().apply()
}
