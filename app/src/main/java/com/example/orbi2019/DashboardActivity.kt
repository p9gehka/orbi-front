package com.example.orbi2019

import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.orbi2019.features.goals.DashboardFragment


class DashboardActivity: AppCompatActivity() {


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        changeGoalFragment(DashboardFragment())

    }
    
    private fun changeGoalFragment(f: Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.dashboard_base_content, f)
        ft.commit()
    }
    override fun onBackPressed() {
        // Do Here what ever you want do on back press;
    }


}