package com.example.orbi2019.models

import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @SerializedName("errors")
    open val errors: List<String>? = null
}


data class UserResponse(
    @SerializedName("id")
    val id: String = ""
) : BaseResponse()

data class GoalResponse(
    @SerializedName("id")
    val id: String = ""
) : BaseResponse()

class GoalItem(
    val id: Int,
    val type: String,
    val autor: String,
    val startTime: String,
    val deadine: String,
    val balance: Int
) {}