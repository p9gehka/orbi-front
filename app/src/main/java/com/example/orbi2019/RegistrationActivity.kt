package com.example.orbi2019

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.orbi2019.networking.IStrokeApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RegistrationActivity : AppCompatActivity() {

    private val urlString = "http://m.p9gehka.xyz/signup/"

    var submitButtonGen:Button?  = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        val editText: EditText = findViewById(R.id.nameField)
        val passwordText = findViewById<EditText>(R.id.passwordField)

        val submitButton = findViewById<Button>(R.id.submit)
        val vkAuthButton = findViewById<Button>(R.id.vkAuthButton)
        submitButtonGen = submitButton

        VK.initialize(applicationContext)

        val myId: Int = getDataInt("VkId",this@RegistrationActivity)
        var authorized = false
        var login = "-1"
        if (myId != -1)
        {
           authorized = true
        } else {
            login = getDataString("login", this@RegistrationActivity)
            if (!login.equals("-1")) {
                authorized = true
            }
        }
        Log.println(Log.ERROR,"[s]","UserId -> " + myId.toString() + " login -> "+login)
        if(authorized)
        {
            val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            Log.println(Log.ERROR,"[s]","UserId -> " + myId.toString())
            startActivity(intent)
        }

        vkAuthButton.setOnClickListener{
            submitButtonGen!!.visibility = View.INVISIBLE
            VK.login(this, arrayListOf(VKScope.WALL, VKScope.PHOTOS, VKScope.FRIENDS, VKScope.GROUPS, VKScope.STATUS, VKScope.EMAIL))
        }

        submitButtonGen!!.setOnClickListener {
            CoroutineScope(Main).launch {
             sendUserData(editText, passwordText)
            }

            val intent = Intent(this, DashboardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            submitButtonGen!!.visibility = View.INVISIBLE
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object: VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                // User passed authorization
                Log.println(Log.ERROR,"[s]","[VK] success auth")
                saveDataVk(token.accessToken, token.userId!!, this@RegistrationActivity)
                CoroutineScope(Main).launch {
                val login:String = "user"+token.userId.toString();
                sendUserDataByVk(login, "vkRegistr228")
                saveUseLogin(login,this@RegistrationActivity);

                }
                val intent = Intent(this@RegistrationActivity, DashboardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)

                startActivity(intent)
            }

            override fun onLoginFailed(errorCode: Int) {
                // User didn't pass authorization
                 this@RegistrationActivity.FailedLoginByVk()
                deleteAllSaveData(this@RegistrationActivity)
                Log.println(Log.ERROR,"[s]","[VK] don't auth")
            }
        }

        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

     fun FailedLoginByVk()
    {
        submitButtonGen!!.visibility = View.VISIBLE
    }

    private suspend fun sendUserData(
        editText: EditText,
        passwordText: EditText
    ) {
        val userResponse = Retrofit.Builder()
            .baseUrl(urlString)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(IStrokeApi::class.java)
            .registerUserAsync(
                editText.text.toString(),
                passwordText.text.toString()
            ).await()
        saveUseLogin(editText.text.toString(),this@RegistrationActivity)
        println(userResponse.id)
    }

    private suspend fun sendUserDataByVk(
        editText: String,
        passwordText: String
    ) {
        val userResponse = Retrofit.Builder()
            .baseUrl(urlString)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(IStrokeApi::class.java)
            .registerUserAsync(
                editText,
                passwordText
            ).await()
        println(userResponse.id)
    }
}
