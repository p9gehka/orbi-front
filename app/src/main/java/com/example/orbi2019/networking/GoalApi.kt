package com.example.orbi2019.networking

import com.example.orbi2019.models.GoalResponse
import kotlinx.coroutines.Deferred

interface GoalApi  {
    suspend fun getGoals(): Deferred<GoalResponse>
}
