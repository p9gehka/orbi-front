package com.example.orbi2019.networking

sealed class ApiResult<out T : Any> {
    sealed class Success<out T: Any> : ApiResult<T>() {
        data class Content<out T : Any>(val data: T) : Success<T>()
        data class Errors(val errorsList: List<String>) : Success<Nothing>()
    }
    data class Failure(val exception: Throwable) : ApiResult<Nothing>()
}