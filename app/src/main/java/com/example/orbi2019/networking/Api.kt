package com.example.orbi2019.networking

import com.example.orbi2019.models.GoalResponse
import com.example.orbi2019.models.UserResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface IStrokeApi {

    @POST("/signup/")
    @FormUrlEncoded
    fun registerUserAsync(
        @Field("login") login: String,
        @Field("password") password: String
    ): Deferred<UserResponse>





}

interface IGoalApi {

    @POST("/goal/add/")
    @FormUrlEncoded
    fun addGoalAsync(
        @Field("type") type: String,
        @Field("author") author: Int,
        @Field("startTime") startTime: String,
        @Field("deadLine") deadLine: String
    ): Deferred<GoalResponse>


}
