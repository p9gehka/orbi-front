package com.example.orbi2019

enum class GoalType {
    REDUCE_SMOKING,
    STOP_SMOKING,
    MORNING_RUNS,
    OTHER
}