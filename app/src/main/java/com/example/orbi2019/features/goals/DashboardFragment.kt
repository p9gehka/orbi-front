package com.example.orbi2019.features.goals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.orbi2019.GoalCreation
import com.example.orbi2019.R
import kotlinx.android.synthetic.main.dashboard_fragment.*

class DashboardFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dashboard_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val ft = fragmentManager?.beginTransaction()
        ft?.replace(R.id.place_dashboard, MyGoalsFragment())
        ft?.commit()

        floatingActionButton4.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.dashboard_base_content,
                GoalCreation())?.addToBackStack(null)
            transaction?.commit()
        }



        mygoals_btn.setOnClickListener {
            val ft = fragmentManager?.beginTransaction()
            ft?.replace(R.id.place_dashboard, MyGoalsFragment())
            ft?.commit()
        }
        friendsgoals_btn.setOnClickListener {

            val ft = fragmentManager?.beginTransaction()
            ft?.replace(R.id.place_dashboard, FriendsGoalsFragment())
            ft?.commit()
        }
    }

}