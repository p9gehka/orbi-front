package com.example.orbi2019.features.goals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orbi2019.R

import com.example.orbi2019.models.GoalItem
import kotlinx.android.synthetic.main.mygoals_fragment.*

class MyGoalsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.mygoals_fragment, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mygoals_list.setHasFixedSize(true)
        mygoals_list.layoutManager = LinearLayoutManager(context)

        initAdapter()

        if (savedInstanceState == null) {
            val items = arrayOf<GoalItem>()
            (mygoals_list.adapter as GoalsAdapter).addGoals(items)
        }
    }
    protected fun initAdapter() {
        if (mygoals_list.adapter == null) {
            mygoals_list.adapter = GoalsAdapter()
        }
    }
}