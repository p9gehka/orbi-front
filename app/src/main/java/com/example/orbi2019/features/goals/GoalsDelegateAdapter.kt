package com.example.orbi2019.features.goals

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.orbi2019.R
import com.example.orbi2019.models.GoalItem


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}
class GoalsDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return TurnsViewHolder(parent)
    }

     fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: GoalItem) {
        holder as TurnsViewHolder
        holder.bind(item)
    }

    class TurnsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.goal_item, false)) {
        fun bind(item: GoalItem) = with(itemView) {
        }
    }
}