package com.example.orbi2019.features.goals

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.orbi2019.models.GoalItem

class GoalsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: ArrayList<GoalItem> = ArrayList()
    private val delegateAdapter = GoalsDelegateAdapter()
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapter.onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return delegateAdapter.onBindViewHolder(holder, this.items[position])
    }

    fun addGoals(goals: Array<GoalItem>) {
        val initPosition = items.size
        items.addAll(goals)
        notifyItemRangeChanged(initPosition, items.size)
    }
}
