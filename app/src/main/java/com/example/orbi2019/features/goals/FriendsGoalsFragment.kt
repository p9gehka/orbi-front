package com.example.orbi2019.features.goals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orbi2019.R
import com.example.orbi2019.models.GoalItem
import kotlinx.android.synthetic.main.friendsgoals_fragment.*

class FriendsGoalsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.friendsgoals_fragment, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        friendsgoals_list.setHasFixedSize(true)
        friendsgoals_list.layoutManager = LinearLayoutManager(context)

        initAdapter()

        if (savedInstanceState == null) {
            val goal = GoalItem(1, "a", "b", "c", "d", 1)
            val items = arrayOf(goal, goal, goal)
            (friendsgoals_list.adapter as GoalsAdapter).addGoals(items)
        }
    }
    protected fun initAdapter() {
        if (friendsgoals_list.adapter == null) {
            friendsgoals_list.adapter = GoalsAdapter()
        }
    }
}